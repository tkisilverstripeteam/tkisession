<?php
/* Tiraki session handler - database
 * Last updated: 2013-06-28
 * @package tkilib
 */
class TkiSession extends Session {
	/* -------- Static variables -------- */
	// Cookie timeout in seconds should be 0 so it expires when browser is closed, 
	// or otherwise much greater than $record_timeout, as it is extended on page visits
	
	protected static $timeout = 0;		// Cookie timeout	
	
	protected static $abs_timeout = 86400;	// Session absolute timeout (seconds)
	
	protected static $idle_timeout = 600;	// Session idle timeout (seconds)
	
	protected static $session_ips = array();
	
	protected static $cookie_name = 'TkiSessionId';
	
	protected static $cookie_domain = null;

	protected static $cookie_path;

	protected static $cookie_secure = false;
	
	protected static $cookie_http = true;
	
	protected static $token_key = 'tkid';
	
	protected static $salt = '';
	
	protected static $default_session = null;
	
	public static $cleanup_number = 100;
	
	public static $enabled = false;
	
	/* -------- Instance variables -------- */
	protected $dataRecord;
	
	/* -------- Static methods -------- */
	
	protected static function set_session_cookie($sid = null) {
		self::load_config();
		$name = self::get_cookie_name();
		$path = self::get_cookie_path();
		$domain = self::get_cookie_domain();
		$secure = self::get_cookie_secure();
		$http = self::get_cookie_http();
		
		if(!headers_sent()) {
			return setcookie($name, $sid, self::get_cookie_expiry(), $path, $domain, $secure, $http);
		} else {
			return false;
		}
	}

	protected static function current_session() {
		if(Controller::has_curr()) {
			return Controller::curr()->getTkiSession();
		} else {
			if(!self::$default_session) {
				$record = self::find_existing_session();
				self::$default_session = new TkiSession($record);
			}
			return self::$default_session;
		}
	}
	
	// Refer to @link Session::set_timeout_ips() 
	public static function set_timeout_ips($session_ips) {
		if(!is_array($session_ips)) {
			user_error("Session::set_timeout_ips expects an array as its argument", E_USER_NOTICE);
			self::$session_ips = array();
		} else {
			self::$session_ips = $session_ips;
		}
	}
	// Refer to @link Session::add_to_array()
	public static function add_to_array($name, $val) {
		return self::current_session()->inst_addToArray($name, $val);
	}
	// Refer to @link Session::set()
	public static function set($name, $val) {
		return self::current_session()->inst_set($name, $val);
	}
	// Refer to @link Session::get()
	public static function get($name) {
		return self::current_session()->inst_get($name);
	}
	// Refer to @link Session::get_all()
	public static function get_all() {
		return self::current_session()->inst_getAll();
	}
	// Refer to @link Session::clear()
	public static function clear($name) {
		return self::current_session()->inst_clear($name);
	}
	// Refer to @link Session::clear_all()
	public static function clear_all() {
		$out = self::current_session()->inst_clearAll();
		self::$default_session = null;
		
		return $out;
	}
	// Refer to @link Session::save()
	public static function save() {
		return self::current_session()->inst_save();
	}
	
	// Refer to @link Session::set_timeout()
	public static function set_timeout($timeout) {
		self::$timeout = intval($timeout);
	}
	// Refer to @link Session::get_timeout()
	public static function get_timeout() {
		return self::$timeout;
	}
	
	public static function get_cookie_expiry() {
		return (intval(self::$timeout) > 0) ? time() + self::$timeout : 0;
	}
	
	public static function set_cookie_name($name) {
		self::$cookie_name = (string) $name;
	}
	
	public static function get_cookie_name() {
		return self::$cookie_name;
	}
	
	// Refer to @link Session::set_cookie_domain()
	public static function set_cookie_domain($domain) {
		self::$cookie_domain = $domain;
	}
	// Refer to @link Session::get_cookie_domain()
	public static function get_cookie_domain() {
		return self::$cookie_domain;
	}
	// Refer to @link Session::set_cookie_path()
	public static function set_cookie_path($path) {
		self::$cookie_path = $path;
	}
	// Refer to @link Session::get_cookie_path()
	public static function get_cookie_path() {
		if(self::$cookie_path) {
			return self::$cookie_path;
		} else {
			return Director::baseURL();
		}
	}
	// Refer to @link Session::set_cookie_secure()
	public static function set_cookie_secure($secure) {
		self::$cookie_secure = (bool) $secure;
	}
	// Refer to @link Session::get_cookie_secure()
	public static function get_cookie_secure() {
		return (bool) self::$cookie_secure;
	}
	
	public static function set_cookie_http($secure) {
		self::$cookie_http = (bool) $secure;
	}
	
	public static function get_cookie_http() {
		return (bool) self::$cookie_http;
	}
	
	public static function create_salt($val) {
		self::$salt = (string) md5($val);
	}
	
	public static function get_salt() {
		return (string) self::$salt;
	}

	/**
	 * Generates a unique ID 32 characters long. Includes a hash of the 
	 * user agent to help avoid session hijacking.
	 * 
	 * @return string
	 */
	protected static function create_session_id() {
		$id = uniqid();	// 13 characters: based on current microtime
		$id .= self::hash_user_agent();	// 14 characters: based on user agent
		$id .= substr(md5(strval(mt_rand(100000,999999))),0,5); // 5 characters: based on random number
		return $id;
	}
	
	protected function fetch_session_record($sid) {
		return DataObject::get_one('TkiSessionData',"\"SesId\" = '$sid'");
	}
	/* 
	 * 
	 * To do: error logging
	 */
	public function find_existing_session() {
		$foundId = self::find_session_id();
		$sid = null;
			// No session id found
		if(empty($foundId)) return null;	
			// Validate session ID
		if(self::valid_session_id($foundId)) {
			$sid = $foundId;
		}
			// Invalid session id
		if(empty($sid)) return null;
			// Get session record
		$record = self::fetch_session_record($sid);
			// No session record
		if(!$record || !$record->exists()) return null;
			// Session expired
		if(self::session_expired($record)) return null;
			// Not expired, so update expiry time and return object
		$record->SesExpires = intval(time() + self::$record_timeout);
		$record->SesData = unserialize($record->SesData);
		$record->write();
		
		return $record;
	}
		// Garbage collection
	public static function delete_expired_sessions() {
		$sqlQuery = new SQLQuery();
		$sqlQuery->from = "TkiSessionData";
		$sqlQuery->where = "\"SesExpires\" < ". time();
		$sqlQuery->delete = true;
		$result = $sqlQuery->execute();
	}
	
	/* Looks for session token in COOKIE,POST,and GET.
	 * Order of precedence: POST,GET, then COOKIE
	 * 
	 */
	protected function find_session_id() {
			// Check request for token
		if(!empty($_POST[self::$token_key])) {
			return $_POST[self::$token_key];
		}
		if(!empty($_GET[self::$token_key])) {
			return $_GET[self::$token_key];
		}
		if(!empty($_COOKIE[self::$cookie_name])) {
			return $_COOKIE[self::$cookie_name];
		}
		return null;
	}
	
	protected static function valid_session_id($id) {
		$out = false;
			// Length test
		$length = strlen($id);
		if(strlen($id) !== 32) return $out;
			// Compare user agent hash in ID to current user agent
		$idBrowser = substr($id,13,14);
		$curBrowser = self::hash_user_agent();
		if(strcmp($idBrowser,$curBrowser) === 0) {
			$out = true;
		}
		return $out;
	}
	
	public static function session_expired($record) {
		return (time() > intval($record->SesExpires));
	}
	
	protected static function find_user_agent() {
		return isset($_SERVER['HTTP_USER_AGENT']) ? (string) $_SERVER['HTTP_USER_AGENT'] : 'JustARandomBrowser';
	}
	
	/**
	 *
	 * @global type $TYPO3_CONF_VARS
	 * @return string  14 character string
	 */
	protected static function hash_user_agent() {
		$userAgent = self::find_user_agent();
		$hash = substr(sha1(self::$salt . $userAgent),0,14);
		return $hash;
	}
	
	/* -------- Instance methods -------- */
	function __construct($data = null) {
		if(!self::$enabled) {
			user_error('TkiSession is not enabled.', E_USER_NOTICE); 
			return;
		}
		
		if($data instanceof Session) { 
			$this->data = $data->inst_getAll();
		} elseif($data instanceof TkiSessionData) {
			$this->dataRecord = $data;
			$this->data = $this->dataRecord->SesData;
		}
		
		if(!$this->dataRecord) $this->createNewRecord(); 

		if($this->dataRecord->exists()) {
				// Set cookie
			$res = self::set_session_cookie($this->dataRecord->SesId); // To do: handle set cookie failure
				
		} else {
			user_error('TkiSession record not created.', E_USER_NOTICE); 
			return;
		}
	
	}
	
	public function createNewRecord() {
		$this->dataRecord = new TkiSessionData();
		$this->dataRecord->SesId = self::create_session_id();
		$this->dataRecord->SesData = $this->data;
		$this->dataRecord->SesExpires = intval(time() + self::$record_timeout);
		$id = $this->dataRecord->write();
			// Garbage collection runs every nth new record
		if($id && (self::$cleanup_number > 0) 
			&& ($id % self::$cleanup_number) === 0) {
			self::delete_expired_sessions();
		}
	}
	/**
	 * Save data to session
	 * Only save the changes, so that anyone manipulating $_SESSION directly doesn't get burned.
	 */ 
	public function inst_save() {
		$this->recursivelyApply($this->changedData, $this->dataRecord->SesData);
		$this->dataRecord->write();
	}
	
	

}

?>