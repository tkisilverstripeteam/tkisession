<?php
/* Tiraki session handler - database
 * Last updated: 2013-06-28
 * @package tkilib
 */
class TkiSessionHandler {
	/* -------- Static variables -------- */
	// Cookie timeout in seconds should be 0 so it expires when browser is closed, 
	// or otherwise much greater than $record_timeout, as it is extended on page visits
	
	protected static $timeout = 0;		// Cookie timeout	
	
	protected static $abs_timeout = 86400;	// Session absolute timeout (seconds)
	
	protected static $idle_timeout = 600;	// Session idle timeout (seconds)
	
	//protected static $session_name = '';
	
	//protected static $cookie_domain = null;

	//protected static $cookie_path;

	//protected static $cookie_secure = false;
	
	//protected static $cookie_http = true;
	
	protected static $token = 'sid';
	
	protected static $salt = '28721a1010d57d0f9185939f03ef6147';
	
	//protected static $default_session = null;
	
	//public static $cleanup_number = 100;
	
	public static $enabled = false;
	
	/* -------- Instance variables -------- */
	protected $dataRecord;
	
	/* -------- Static methods -------- */

    protected $savePath;
    protected $sessionName;

    public function __construct() {
        session_set_save_handler(
            array($this, "open"),
            array($this, "close"),
            array($this, "read"),
            array($this, "write"),
            array($this, "destroy"),
            array($this, "clean")
        );
    }
	/* -------- Main session methods -------- */
    public function open($savePath, $sessionName) {
        $this->savePath = $savePath;
        $this->sessionName = $sessionName;
        return true;
    }

    public function close() {
        
        return true;
    }

    public function read($sid) {
       if(self::valid_session_id($sid)) {
			$sid = $foundId;
		}
			// Invalid session id
		if(empty($sid)) return null;
			// Get session record
		$record = self::fetch_session_record($sid);
			// No session record
		if(!$record || !$record->exists()) return null;
			// Session expired
		if(self::session_expired($record)) return null;
			// Not expired, so update expiry time and return object
		$record->SesExpires = intval(time() + self::$record_timeout);
		$record->SesData = unserialize($record->SesData);
		$record->write();
		
		return $record;
    }
	
    public function write($sid, $data) {
       
    }

    public function destroy($sid) {
       
    }

    public function clean($maxlifetime) {
       
    }
	
	/* -------- Session utility methods -------- */
	/**
	 * Generates a unique ID 32 characters long. Includes a hash of the 
	 * user agent to help avoid session hijacking.
	 * 
	 * @return string
	 */
	protected static function create_session_id() {
		$id = uniqid();	// 13 characters: based on current microtime
		$id .= self::hash_user_agent();	// 14 characters: based on user agent
		$id .= substr(md5(strval(mt_rand(100000,999999))),0,5); // 5 characters: based on random number
		return $id;
	}
	
	public static function session_expired($record) {
		return (time() > intval($record->SesExpires));
	}
	
	protected static function valid_session_id($id) {
		$out = false;
			// Length test
		$length = strlen($id);
		if(strlen($id) !== 32) return $out;
			// Compare user agent hash in ID to current user agent
		$idBrowser = substr($id,13,14);
		$curBrowser = self::hash_user_agent();
		if(strcmp($idBrowser,$curBrowser) === 0) {
			$out = true;
		}
		return $out;
	}
	
	protected function fetch_session_record($sid) {
		$sid = Convert::raw2sql($sid);
		return DataObject::get_one('TkiSessionData',"\"SesId\" = '$sid'");
	}
	
	protected static function find_user_agent() {
		return isset($_SERVER['HTTP_USER_AGENT']) ? (string) $_SERVER['HTTP_USER_AGENT'] : 'JustARandomBrowser';
	}
	
	/**
	 *
	 * @global type $TYPO3_CONF_VARS
	 * @return string  14 character string
	 */
	protected static function hash_user_agent() {
		$userAgent = self::find_user_agent();
		$hash = substr(sha1(self::$salt . $userAgent),0,14);
		return $hash;
	}
	
}



?>