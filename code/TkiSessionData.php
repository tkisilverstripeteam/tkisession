<?php
/* Tiraki session data object
 * Last updated: 2013-06-28
 * @package tkilib
 */
class TkiSessionData extends DataObject {
	/* -------- Static variables -------- */
	public static $db = array(
		'SesId' => 'Varchar(32)',
		'SesData' => 'TkiMediumBlob'
	);
	
	public static $has_one = array(
		'Member' => 'Member'
	);

	public static $has_many = array();

	public static $many_many = array();

	public static $belongs_many_many = array();
	
	static $indexes = array(
		"SesId" => true,
	);
	
	public static $extensions = array();
	
	public static $singular_name = 'Session Data';
	public static $plural_name = 'Session Data';
	
	/* -------- Instance variables -------- */
	
	/* -------- Static methods -------- */
	
	/* -------- Instance methods -------- */
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		return $fields;
	}

	public function onBeforeWrite() {
			// Save member ID, if logged in
		$currentMember = Member::currentMember();
		if(!$this->MemberID && $currentMember) $this->MemberID = $currentMember->ID;
		parent::onBeforeWrite();
	}
	
	public function saveSesData($data) {
		$this->SesData = serialize($data);
	}
	
	public function SesData() {
		return unserialize($this->SesData);
	}
	
	/* ---- Permissions ---- */
	public function canEdit($member = null) {
		return false;
	}

	public function canDelete($member = null) {
		return $this->canEdit($member);
	}

	public function canCreate($member = null) {
		return $this->canEdit($member);
	}

}

?>